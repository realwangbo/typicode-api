package typicode.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Comment {
  @Id
  private String id;
  private String postId;
  private String name;
  private String email;
  private String body;
}
