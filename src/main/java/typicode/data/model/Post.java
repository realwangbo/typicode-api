package typicode.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class Post {
  @Id
  private String id;
  private String userId;
  private String title;
  private String body;
}
