package typicode.data.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CommentDto {
  private String id;
  private String postId;
  private String name;
  private String email;
  private String body;
}
