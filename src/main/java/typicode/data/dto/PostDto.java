package typicode.data.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PostDto {
  private String id;
  private String userId;
  private String title;
  private String body;
  private List<CommentDto> comments = new ArrayList<>();
}
