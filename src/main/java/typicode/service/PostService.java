package typicode.service;

import typicode.data.dto.PostDto;

import java.util.List;

public interface PostService {

  List<PostDto> getPosts();
}
