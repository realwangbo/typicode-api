package typicode.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import typicode.data.dto.CommentDto;
import typicode.data.dto.PostDto;
import typicode.data.model.Comment;
import typicode.data.model.Post;
import typicode.external.TypicodeClient;
import typicode.mapper.DataMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {

  @Autowired
  DataMapper dataMapper;

  @Autowired
  TypicodeClient typicodeClient;

  @Override
  public List<PostDto> getPosts() {
    List<Post> posts = typicodeClient.getPosts();
    List<Comment> comments = typicodeClient.getComments();
    List<PostDto> result = new ArrayList<>();
    Optional.ofNullable(posts).orElseGet(ArrayList::new)
      .stream()
      .forEach(p -> {
        PostDto postDto = dataMapper.postToPostDto(p);
        setComments(postDto, comments);
        result.add(postDto);
      });
    return result;
  }

  private void setComments(PostDto postDto, List<Comment> comments) {
    Optional.ofNullable(comments).orElseGet(ArrayList::new)
      .stream()
      .filter(o -> Objects.equals(o.getPostId(), postDto.getId()))
      .forEach(o -> {
        CommentDto commentDto = dataMapper.commentToCommentDto(o);
        postDto.getComments().add(commentDto);
      });
  }
}