package typicode.utils;

public class Constants {

  /* ERROR CODES */
  public static final String TYPICODE_ERROR_CODE = "TYPICODE_API_0001: ";

  private Constants() {
    throw new IllegalStateException("Utility class");
  }
}
