package typicode.external;

import typicode.exception.TypicodeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import typicode.config.AppConfig;
import typicode.data.model.Comment;
import typicode.data.model.Post;

import java.net.URI;
import java.util.*;

import static org.springframework.util.ObjectUtils.isEmpty;

@Service
public class TypicodeClientImpl implements TypicodeClient {

  private static final Logger LOGGER = LoggerFactory.getLogger(TypicodeClientImpl.class);

  @Autowired
  RestTemplate restTemplate;

  @Autowired
  AppConfig appConfig;

  private static URI getSearchUrl(String transactionUrl) {
    UriComponentsBuilder builder = UriComponentsBuilder
      .fromUriString(transactionUrl);
    return builder.build().encode().toUri();

  }

  @Override
  public List<Post> getPosts() {
    try {
      URI getPostsUri = getSearchUrl(appConfig.getPostsPath());

      Post[] posts = restTemplate.getForObject(getPostsUri, Post[].class);

      if (!isEmpty(posts)) {
        LOGGER.info("Received {} Posts", posts.length);
        return Arrays.asList(posts);
      } else {
        LOGGER.info("No Posts received");
        return new ArrayList<>();
      }
    } catch (RuntimeException e) {
      throw new TypicodeException(e);
    }
  }

  @Override
  public List<Comment> getComments() {
    try {
      URI getCommentsUri = getSearchUrl(appConfig.getCommentsPath());

      Comment[] comments = restTemplate.getForObject(getCommentsUri, Comment[].class);

      if (!isEmpty(comments)) {
        LOGGER.info("Received {} Comments", comments.length);
        return Arrays.asList(comments);
      } else {
        LOGGER.info("No Comments received");
        return new ArrayList<>();
      }
    } catch (RuntimeException e) {
      throw new TypicodeException(e);
    }
  }
}