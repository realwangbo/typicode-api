package typicode.external;

import typicode.data.model.Comment;
import typicode.data.model.Post;

import java.util.List;

public interface TypicodeClient {

  List<Post> getPosts();

  List<Comment> getComments();
}
