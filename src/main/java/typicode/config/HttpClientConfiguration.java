package typicode.config;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.integration.annotation.Default;
import org.springframework.web.client.RestTemplate;

import java.util.function.Supplier;

@Configuration
public class HttpClientConfiguration {

  @Autowired
  private AppConfig settings;

  /**
   * Configure a container managed instance of ClientHttpRequestFactory which uses connection pooling.
   *
   * @return instance of ClientHttpRequestFactory.
   */
  @Bean
  public CloseableHttpClient httpClient() {
    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
    connectionManager.setMaxTotal(settings.getMaxTotal());
    connectionManager.setDefaultMaxPerRoute(settings.getMaxRoute());

    RequestConfig config = RequestConfig.custom()
      .setConnectTimeout(settings.getConnectionTimeOut())
      .setConnectionRequestTimeout(settings.getConnectionRequestTimeout())
      .setSocketTimeout(settings.getSocketTimeout())
      .build();

    CloseableHttpClient httpClient = HttpClientBuilder.create()
      .setConnectionManager(connectionManager)
      .setDefaultRequestConfig(config)
      .build();

    return httpClient;
  }

  /**
   * Spring RestTemplate for communication.
   *
   * @param builder instance of {@link RestTemplateBuilder}
   * @return RestTemplate
   */
  @Bean
  @Default
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory(httpClient());
    Supplier<ClientHttpRequestFactory> supplier = () -> new BufferingClientHttpRequestFactory(factory);
    return builder.requestFactory(supplier).build();
  }
}
