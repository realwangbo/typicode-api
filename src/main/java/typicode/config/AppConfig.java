package typicode.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class AppConfig {

  @Value(value = "${typicodeApi.postsApi.postsPath}")
  private String postsPath;

  @Value(value = "${typicodeApi.commentsApi.commentsPath}")
  private String commentsPath;

  @Value("${settings.connection.maxTotal:500}")
  private Integer maxTotal;
  @Value("${settings.connection.maxRoute:20000}")
  private Integer maxRoute;
  @Value("${settings.connection.connectionTimeOut:10000}")
  private Integer connectionTimeOut;
  @Value("${settings.connection.connectionRequestTimeout:10000}")
  private Integer connectionRequestTimeout;
  @Value("${settings.connection.socketTimeout:10000}")
  private Integer socketTimeout;

}
