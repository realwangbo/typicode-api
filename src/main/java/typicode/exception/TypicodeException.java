package typicode.exception;

import static typicode.utils.Constants.TYPICODE_ERROR_CODE;

public class TypicodeException extends ApiException {
  public TypicodeException(Exception ex) {
    super(TYPICODE_ERROR_CODE.concat(ex.getMessage()));
  }
}