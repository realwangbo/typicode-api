package typicode.mapper;

import org.mapstruct.Mapper;
import typicode.data.dto.CommentDto;
import typicode.data.dto.PostDto;
import typicode.data.model.Comment;
import typicode.data.model.Post;

@Mapper(componentModel = "spring")
public interface DataMapper {

  Post postDtoToPost(PostDto dto);

  PostDto postToPostDto(Post post);

  Comment commentDtoToComment(CommentDto dto);

  CommentDto commentToCommentDto(Comment comment);
}