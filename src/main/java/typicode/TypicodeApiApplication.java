package typicode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "classpath:build.properties")
public class TypicodeApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(TypicodeApiApplication.class, args);
  }
}
