package typicode.controller;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import typicode.data.dto.PostDto;
import typicode.service.PostService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.springframework.util.ObjectUtils.isEmpty;

@RestController
@RequestMapping(value = "/posts")
public class PostController {

  @Autowired
  private PostService postService;

  private final Logger LOGGER = LoggerFactory.getLogger(getClass());

  @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Get all Posts", response = List.class)
  public ResponseEntity getPosts() {
    LOGGER.info("Request received for querying all Posts");
    List<PostDto> posts = Optional.ofNullable(postService.getPosts()).orElseGet(ArrayList::new);
    if (isEmpty(posts)) {
      return ResponseEntity.noContent().build();
    } else {
      return ResponseEntity.ok(posts);
    }
  }

}