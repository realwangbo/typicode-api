package typicode.external;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import typicode.config.AppConfig;
import typicode.data.model.Comment;
import typicode.data.model.Post;
import typicode.exception.TypicodeException;

import java.net.URI;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TypicodeClientTest {

  @InjectMocks
  TypicodeClientImpl typicodeClient;

  @Mock
  RestTemplate restTemplate;

  @Mock
  AppConfig appConfig;

  @Before
  public void setUp() {
    given(appConfig.getPostsPath()).willReturn("http://localhost:5005/posts");
    given(appConfig.getCommentsPath()).willReturn("http://localhost:5005/comments");
  }

  @Test
  public void getPosts() {
    typicodeClient.getPosts();
    URI postUri = URI.create("http://localhost:5005/posts");
    verify(restTemplate, times(1)).getForObject(eq(postUri), eq(Post[].class));
  }

  @Test(expected = TypicodeException.class)
  public void getPosts_exception() {
    doThrow(new RuntimeException("Error")).when(restTemplate).getForObject(any(URI.class), eq(Post[].class));
    typicodeClient.getPosts();
  }

  @Test
  public void getComments() {
    typicodeClient.getComments();
    URI commentUri = URI.create("http://localhost:5005/comments");
    verify(restTemplate, times(1)).getForObject(eq(commentUri), eq(Comment[].class));
  }

  @Test(expected = TypicodeException.class)
  public void getComments_exception() {
    doThrow(new RuntimeException("Error")).when(restTemplate).getForObject(any(URI.class), eq(Comment[].class));
    typicodeClient.getComments();
  }
}