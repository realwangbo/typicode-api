package typicode.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import typicode.data.dto.CommentDto;
import typicode.data.dto.PostDto;
import typicode.data.model.Comment;
import typicode.data.model.Post;
import typicode.external.TypicodeClient;
import typicode.mapper.DataMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceTest {

  @InjectMocks
  PostServiceImpl postService;

  @Mock
  TypicodeClient typicodeClient;

  @Mock
  DataMapper dataMapper;

  @Test
  public void testGetPosts_empty() {
    given(typicodeClient.getPosts()).willReturn(new ArrayList<>());
    List<PostDto> posts = this.postService.getPosts();
    verify(typicodeClient, times(1)).getPosts();
    verify(typicodeClient, times(1)).getComments();
    assertEquals(0, posts.size());
  }

  @Test
  public void testGetPosts() {
    Post post = new Post();
    post.setId("1");
    Comment comment = new Comment();
    comment.setPostId("1");

    PostDto postDto = new PostDto();
    postDto.setId("1");
    CommentDto commentDto = new CommentDto();
    commentDto.setPostId("1");

    given(typicodeClient.getPosts()).willReturn(Arrays.asList(post));
    given(typicodeClient.getComments()).willReturn(Arrays.asList(comment));
    given(dataMapper.postToPostDto(post)).willReturn(postDto);
    given(dataMapper.commentToCommentDto(comment)).willReturn(commentDto);
    List<PostDto> posts = this.postService.getPosts();
    verify(typicodeClient, times(1)).getPosts();
    verify(typicodeClient, times(1)).getComments();
    assertEquals(1, posts.size());
    assertEquals(1, posts.get(0).getComments().size());
  }
}