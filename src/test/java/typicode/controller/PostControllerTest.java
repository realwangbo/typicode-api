package typicode.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import typicode.data.dto.PostDto;
import typicode.service.PostService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class PostControllerTest {

  private static final String BASE_API_URL = "/posts";

  @Mock
  private PostService postService;
  @InjectMocks
  private PostController controller;

  private MockMvc mvc;
  private List<PostDto> posts;

  @Before
  public void setUp() {
    this.mvc = standaloneSetup(controller).build();
    posts = new ArrayList<>();
    posts.add(populatePostDto("1"));
  }

  private PostDto populatePostDto(String id) {
    PostDto postDto = new PostDto();
    postDto.setId(id);
    return postDto;
  }

  @Test
  public void testGetPosts_OK() throws Exception {
    given(this.postService.getPosts()).willReturn(posts);
    this.mvc.perform(get(BASE_API_URL).accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isOk());
  }

  @Test
  public void testGetPosts_204() throws Exception {
    given(this.postService.getPosts()).willReturn(null);
    this.mvc.perform(get(BASE_API_URL).accept(MediaType.APPLICATION_JSON))
      .andExpect(status().isNoContent());
  }
}
