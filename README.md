# Typicode API
This repository contains source code for the Restful API to fetch data from Typicode.

## Requirements
- Java 8+ (runtime environment)
- Maven 3+ (optional, to build the application)
- IntelliJ CE 2020.3+ (optional)
- Set JAVA_HOME properly

## Quick Start
An executable ```typicode-api-1.0.0.jar``` file has been uploaded in the root folder (usually we don't do this in work), just for your convenience. 
If you'd like to run the api quickly, just run ```java -jar typicode-api-1.0.0.jar``` from the root folder and ignore the below instructions.

## Build
Run ```mvn clean install``` under the root folder of this repository

## Build Artefact
The build produces an executable ```typicode-api-1.0.0.jar``` file under the target folder

## Run
Run the application using the java -jar command.
On a successful startup, the API will listen on port `5004`.

Example: ``` java -jar target/typicode-api-1.0.0.jar```

## Stop
- ```Ctrl+C``` on the terminal will gracefully shut down the application
- Killing the relevant java process will also gracefully shut down the application
  
  Example: ```kill 1234``` where 1234 is the java process id

## Swagger API Docs
On a successful startup go to `http://localhost:5004/typicode-api/swagger-ui.html` to view the API docs and try it out yourself.